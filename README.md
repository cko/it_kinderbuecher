# Materialsammlung Kinderbücher über Open Source, Datenschutz, Internet, Software, etc.

## Was sollten Kinder über IT wissen?
* [Gesellschaft für Informatik: Kompetenzen für informatische Bildung im Primarbereich](https://dl.gi.de/items/33104231-7fc7-4c5a-80b6-141bff8c3304)
* [Bayerischer Bildungs- und Erziehungsplan - Seite 218-230](https://www.ifp.bayern.de/veroeffentlichungen/books/bildungs-erziehungsplan)
* [FRÖBEL (Träger von Kinderkrippen, Kindergärten und Horten): Digitale Medien und Kinder]( https://www.froebel-gruppe.de/fileadmin/user/Dokumente/Broschueren_Themenhefte/Themenheft_Digitale_Medien_2020.pdf )
* [Kultusministerkonferenz: Bildung in der digitalen Welt“ - Seite 10-13](https://www.kmk.org/fileadmin/Dateien/pdf/PresseUndAktuelles/2017/Digitalstrategie_KMK_Weiterbildung.pdf)
* [Comp@ss Computerführerschein](http://www.compass-deutschland.net/)
* [The Computing Curriculum by the Raspberry Pi Foundation](https://www.raspberrypi.org/curriculum)


## Bücher
### Open Source Kinderbücher
* [Ada & Zangemann - Ein Märchen über Software, Skateboards und Himbeereis - freies Bilderbuch von der FSFE](https://fsfe.org/activities/ada-zangemann/index.de.html) [(Source)](https://git.fsfe.org/FSFE/ada-zangemann)

### Bücher/Broschüren von Institutionen
* [Pixi Büchlein vom Bundesbeauftragten für Datenschutz](https://www.bfdi.bund.de/DE/Service/Publikationen/Pixi/Pixi_node.html)
* [Ins Internet - mit Sicherheit! Ein Mal- und Rätselheft für Nachwuchsnerds vom BSI](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/Broschueren/Wegweiser_Checklisten_Flyer/Malbuch_Raetselheft.pdf?__blob=publicationFile&v=8)
* [Unsere Daten – das neue Gold?! - Broschüre von der Landeszentrale für politische Bildung Baden-Württemberg](https://www.lpb-bw.de/publikation-anzeige/mk-55-2023-unsere-daten-das-neue-gold-3662)

### Kommerzielle Bücher
* [Hello Ruby](https://www.helloruby.com/de)
    * Programmier dir deine Welt
    * Die Reise ins Innere des Computers
    * Expedition ins Internet
    * Wenn Roboter zur Schule gehen
    

### Bücherempfehlungen
* [FRÖBEL: Digitale Medien und Kinder Seite 43]( https://www.froebel-gruppe.de/fileadmin/user/Dokumente/Broschueren_Themenhefte/Themenheft_Digitale_Medien_2020.pdf )
* [App Camps Blog](https://appcamps.de/2021/09/02/8-kinderbuecher-fuer-die-ersten-schritte-in-der-informatik/)

## Comics & Illustrationen
* https://github.com/searchableguy/awesome-illustrated-guides#internet
* [Programming zines von Julia Evans (einige sind kostenlos)](https://wizardzines.com/)

## Bücher selber erstellen
IT-Bilderbuch
* [Bilderbuch drucken (cewe/Rossmann)](https://cewe.rossmann-fotowelt.de/cewe-fotobuch/kinder.html)
* [Entwurf Inhalt](./bilderbuch.md)

## Weitere Projekte für Kinder
Linux-Distributionen für Kinder
* [JUXlala](https://www.bibernetz.de/wws/juxlala.html)
* [DoudouLinux](https://www.doudoulinux.org/web/deutsch/index.html)
* [Icefun](https://gnulinux.ch/icefun-linux-f%C3%BCr-kids)

Lernsoftware
* [GCompris (freie Lernsoftware für Kinder)](https://www.gcompris.net/index-de.html)

Webangebote
* [internet-abc: Verein, der Kinder, Eltern und Lehrkräfte unterstützt, ein besseres Verständnis für die Chancen und Herausforderungen des Internets zu entwickeln](https://www.internet-abc.de/)
* [Sendung mit der Maus](https://www.wdrmaus.de/extras/mausthemen/digitalisierung/index.php5)


