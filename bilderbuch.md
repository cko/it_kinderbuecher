# Erster Wortschatz: Digitale Welt
Plan: Zunächst für jede Doppelseite 10 mögliche Motive finden,
anschließend eine Auswahl von 4-8 Fotos finden/erstellen.

Schriftart: Andika mit Fibel a

## Deckblatt

## Seite 2 & 3: Computer zu Hause
* Smartphone
* Tablet
* Laptop 
* PC (+ Maus, Tastatur, ...)
* Smart Watch
* Smart TV
* Digitale Assistenten
* Fitness Tracker
* E-Book Reader
* Heimserver
* USB-Stick/externe Festplatte

## Seite 4 & 5: Filmen, Drucken, Schneiden, Fräsen ...
* Headset/Kopfhörer/Mikrofon
* Stream Deck
* Kamera (+Speicherkarte)
* Drucker
* 3D Drucker
* Schneidplotter
* Lasercutter
* CNC-Fräse
* Stickmaschine
* Strickmaschine
* Digitales Mikroskop
* Taschenbeamer

## Seite 6 & 7: Anwendungen
* Navigation
* Wetter
* Malen
* Fotografieren & Filmen
* Telefonieren/Videotelefonie
* Photos & Videos anschauen & bearbeiten
* Sprachnachrichten
* Einkaufen
* Programmieren
* Monitoring Dashboards
* Natur digital erforschen

## Seite 8 & 9: Strom
* USB A&C Stecker
* Stromkabel/-stecker
* Akkus
* Steckdosen
* Netzteile
* Powerbanks
* Mobiles Solarpanel
* induktives Laden

## Seite 10 & 11: IT im Alltag 
* Geldautomat
* Supermarktkasse
* Informationssystem im Auto
* Informationssystem in der S-Bahn
* Abfahrtsanzeigen an U-Bahn/Bus/Strassenbahn/Bahnhof
* Park- und Fahrkartenautomat
* Paketstation
* Krankenkassenkarte
* Biblotheksausweis
* Schwimmbadkarte
* Museumskarte

## Seite 12 & 13: Roboter
* Industrieroboter (Schweißen, Montage, Lackieren, Verpacken)
* Humanoider Roboter
* Agrarroboter
* Hamburger Hafen automatisierte Containertransporte
* Tauchroboter
* Lagerroboter
* Staugsauger-/Wischroboter
* Restaurantroboter
* Drohnen
* Rasenmäherroboter

## Seite 14 & 15: Funk
* RFID Tags(Logistik, Inventarverwaltung, Zutrittskontrolle, Identifizierung, MIFARE)
* NFC (Tonies, tiptoi, berührungsloses Bezahlen)
* Bluetooth (Beacons)
* W-LAN (W-LAN Router, Antenne)
* Mobilfunk (Basisstation, LTE-Antenne)
* Sonstige (Satellit/-empfänger, GPS, Richtfunk)
* Spektrumsanalyzer

## Seite 16 & 17: Internetinfrastruktur: Kabel
* In Gebäuden
  * Glasfaserübergabepunkt
  * optischer Wandler
  * Glasfaser Kabel/Stecker/Dose 
  * LAN Kabel/Stecker/Dose
  * Switch/Patch Panel/Router
* Draussen
  * Kabelverzweiger
  * Unterseekabel
  * Unter/-oberirdische Kabel
* Werkzeuge
  * Werkzeug Crimpzangen
  * Gerät zum spleissen


## Seite 18 & 19: Internetinfrastruktur: Im Rechenzentrum
* Rechenzentrum von aussen
* Reihe von Racks
* Einzelner Server
* Doppelboden
* Lüftung
* Notstromaggregat
* (Patchpanel)
* (Switch)
* (Router)
* Techniker


## Seite 20 & 21:  Platinen & Elektronik
* Seite 20 - Beispiele: Fairphone Module, Raspberry Pi, Notebook Mainboard, Innenleben eines USB-Sticks
* Seite 21:
    * Werkzeuge: Lötkolben, Durchgangsprüfer,  
    * Bauelemente: SMD vs. Durchsteck-Bauteile, Kondensator, Widerstände, Dioden
    * CPU/Mikroprozessoren
    * Kühler
    * Unbestückte Platine
* Fotos
  * Raspberry Pi: https://commons.wikimedia.org/wiki/File:23551-raspberry-pi-5.jpg
  * USB-Stick: https://commons.wikimedia.org/wiki/File:VLI_VL751_Chip_Board_(5976873457).jpg


## Schlussseite
* Bildnachweis
* Link
        
